Source: libcommoncpp2
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               doxygen,
               dpkg-dev (>= 1.13.19),
               libxml2-dev,
               zlib1g-dev
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://www.gnu.org/software/commoncpp/
Vcs-Git: https://salsa.debian.org/debian/libcommoncpp2.git
Vcs-Browser: https://salsa.debian.org/debian/libcommoncpp2

Package: libcommoncpp2-dev
Section: libdevel
Architecture: any
Depends: libccgnu2-1.8-0v5 (= ${binary:Version}),
         pkg-config,
         zlib1g-dev,
         ${misc:Depends}
Suggests: libcommoncpp2-doc
Description: Header files and static libraries for Common C++ "2"
 Common C++ is a GNU package which offers portable "abstraction" of system
 services such as threads, networks, and sockets.  Common C++ also offers
 individual frameworks generally useful to developing portable C++
 applications including a object persistence engine, math libraries,
 threading, sockets, etc.  Common C++ is small, and highly portable.
 Common C++ will support most Unix operating systems as well
 as Win32, in addition to GNU/Linux.
 .
 This package contains the development files.

Package: libccgnu2-1.8-0v5
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Conflicts: libccgnu2-1.8-0
Replaces: libccgnu2-1.8-0
Description: GNU package for creating portable C++ programs
 Common C++ "2" is a GNU package which offers portable "abstraction"
 of system services such as threads, networks, and sockets.  Common
 C++ also offers individual frameworks generally useful to developing
 portable C++ applications including a object persistence engine, math
 libraries, threading, sockets, etc.  Common C++ is small, and highly
 portable.  Common C++ will support most Unix operating systems as
 well as Win32, in addition to GNU/Linux.
 .
 This package contains the runtime libraries.

Package: libcommoncpp2-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Documentation files for Common C++ "2"
 Common C++ "2" is a GNU package which offers portable "abstraction"
 of system services such as threads, networks, and sockets.  Common
 C++ also offers individual frameworks generally useful to developing
 portable C++ applications including a object persistence engine, math
 libraries, threading, sockets, etc.  Common C++ is small, and highly
 portable.  Common C++ will support most Unix operating systems as
 well as Win32, in addition to GNU/Linux.
 .
 This package contains the library development documentation.
