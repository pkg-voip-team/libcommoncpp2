libcommoncpp2 (1.8.1-11) UNRELEASED; urgency=medium

  * Drop transition for old debug package migration.
  * Update standards version to 4.6.0, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 02 Oct 2021 23:50:21 -0000

libcommoncpp2 (1.8.1-10) unstable; urgency=medium

  * QA upload.
  * debian/rules: Remove oddly named manpages with little
    content. (Closes: #857380)

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Sun, 27 Dec 2020 01:32:27 -0800

libcommoncpp2 (1.8.1-9) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.5.0, no changes needed.

  [ Vagrant Cascadian ]
  * debian/rules: Remove example Makefile. (Closes: #978407)
  * debian/control: Update Vcs headers.
  * debian/rules: Rename manpages based on build paths. (Closes: #978408)
  * libcommoncpp2-doc: Refresh lintian overrides.
  * debian/control: Set Rules-Requires-Root to "no".
  * debian/control: Update to debhelper-compat 13.
  * debian/rules: Remove --list-missing argument from dh_install.
  * Fix installation of commoncpp2.info
  * debian/control: Update Standards-Version to 4.5.1.

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Sat, 26 Dec 2020 19:01:39 -0800

libcommoncpp2 (1.8.1-8) unstable; urgency=medium

  * QA upload.
  * Fix ftbfs with gcc-9. (Closes: #925739)
  * Update Standards-Version to 4.4.1

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Sat, 28 Dec 2019 00:36:16 +0000

libcommoncpp2 (1.8.1-7) unstable; urgency=medium

  * QA upload.
  * Use debhelper v11
  * Remove dh options which are no longer needed
  * Remove unused (build-)dependencies: gnutls/gcrypt.
    Thanks to Andreas Metzler <ametzler@debian.org> (Closes: #864086, #912025)
  * Update Vcs-* URLs to salsa.d.o
  * Drop package libcommoncpp2-dbg and use automatic dbgsym migration
  * Use Multi-Arch: foreign for libcommoncpp2-doc
  * Update doc-base paths
  * Remove unneeded Build-Depends: autotools-dev

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 31 Oct 2018 15:33:26 +0100

libcommoncpp2 (1.8.1-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++5 ABI transition (closes: 791107).

 -- Julien Cristau <jcristau@debian.org>  Sun, 16 Aug 2015 17:39:25 +0200

libcommoncpp2 (1.8.1-6) unstable; urgency=low

  * Hardcode Build-Depends: libgnutls28-dev
    - Fixes "twinkle: indirectly depends on multiple gnutls, ucommon
    versions" (Closes: #725020)
  * Update Vcs:
  * Update Standards Version: 3.9.5
  * Build --with autotools_dev to update config.{sub,guess} for arm64.
    - Thanks William Grant <wgrant@ubuntu.com>
    - Fixes "update config.{sub,guess} for the AArch64 port" (Closes: #727400)
  * Apply inaddr.patch from Tristan Matthews
    - Fixes "crash from buffer overflow" (Closes: #693471)

 -- Mark Purcell <msp@debian.org>  Sun, 29 Dec 2013 14:49:30 +1100

libcommoncpp2 (1.8.1-5) unstable; urgency=low

  * wheezey polish

  * Update Standards Version 3.9.3
  * debian/compat -> 9 hardening and multiarch
  * Added spelling-error-in-manpage.patch
  * minor lintian cleanups

 -- Mark Purcell <msp@debian.org>  Sat, 23 Jun 2012 16:02:03 +1000

libcommoncpp2 (1.8.1-4) unstable; urgency=low

  * Fix "FTBFS on kfreebsd-*: sys/kern/types.h:87:17: error: expected
    unqualified-id before 'char'" patch from Robert Millan
    (Closes: #673530)

 -- Mark Purcell <msp@debian.org>  Sat, 19 May 2012 11:03:29 +1000

libcommoncpp2 (1.8.1-3) unstable; urgency=low

  * source/format -> 3.0 quilt
  * Switch to dh - drop cdbs/ dpatch
  * Fix gcc-4.7 FTBFS debian/patches/applog-inc-fcntl.patch
  * Fix "Getting rid of unneeded *.la / emptying dependency_libs"
    added overide_dh_install (Closes: #633280)
  * Fix "overwriting const argument in cidr::set" patch from Stefan Potyra
    (Closes: #547352)

 -- Mark Purcell <msp@debian.org>  Sat, 19 May 2012 10:40:33 +1000

libcommoncpp2 (1.8.1-2) unstable; urgency=low

  * Upload to unstable - coordinated through debian-release

 -- Mark Purcell <msp@debian.org>  Tue, 15 May 2012 08:16:35 +1000

libcommoncpp2 (1.8.1-1) experimental; urgency=low

  * New upstream release
  * Drop cstdlib_sparc.dpatch - included upstream

 -- Mark Purcell <msp@debian.org>  Sat, 29 Jan 2011 18:05:27 +1100

libcommoncpp2 (1.8.0-1) experimental; urgency=low

  * New upstream release
  * NEW package libccgnu2-1.8-0 soname-match
  * NEW Package libcommoncpp2-dbg (Closes: #532780)

  [ Kilian Krause ]
  * Remove -N from wget args in get-orig-source target as -O is already
    used.

  [ Mark Purcell ]
  * Removed const_cast_conversion.dpatch included upstream
  * Lintian cleanups:
    - patch-system-but-no-source-readme
    - duplicate-long-description
    - copyright-with-old-dh-make-debian-copyright (Closes: #532784)
    - out-of-date-standards-version

 -- Mark Purcell <msp@debian.org>  Sun, 07 Mar 2010 04:15:23 +1100

libcommoncpp2 (1.7.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS due to invalid constant string to char* conversion in
    src/cidr.cpp (added const_cast_conversion.dpatch) (Closes: #560463)

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 01 Feb 2010 15:16:29 +0100

libcommoncpp2 (1.7.3-1) unstable; urgency=low

  * New upstream release
  * Bump debian/compat -> 7
  * Remove (c) -fixes:copyright-with-old-dh-make-debian-copyright
  * Update debian/rules help2man -> debian/libcommoncpp2-dev.manpages

 -- Mark Purcell <msp@debian.org>  Thu, 02 Apr 2009 22:22:50 +1100

libcommoncpp2 (1.7.1-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright

 -- Mark Purcell <msp@debian.org>  Mon, 16 Feb 2009 22:22:47 +1100

libcommoncpp2 (1.7.0-1) experimental; urgency=low

  * New upstream release
  * NEW package libccext2-1.7-0
    - fixes package-name-doesnt-match-sonames
  * debian/libcommoncpp2-doc.lintian-overrides - cover up doxygen
  * Add ${misc:Depends}

 -- Mark Purcell <msp@debian.org>  Tue, 23 Dec 2008 16:36:07 +1100

libcommoncpp2 (1.6.3-1) experimental; urgency=low

  * New Upstream Release

  [ Patrick Matthäi ]
  * Bumped to Standards-Version 3.8.0.

  [ Mark Purcell ]
  * Debian patches included upstream
    - 01_debian.dpatch
    - no-include-bits_atomicity.h.dpatch
    - gnutls.dpatch

 -- Mark Purcell <msp@debian.org>  Sun, 09 Nov 2008 21:07:01 +1100

libcommoncpp2 (1.6.2-2) unstable; urgency=high

  * Upload to fix FTBFS on sparc thus, urgency=high
  * add dpatch to add missing include on src/mempager.cpp (Closes: #477505)
    Thanks for Jurij Smakov & Martin Zobel-Helas for the patch.
  * lintian cleanup:
    - dpatch-missing-description: no-include-bits_atomicity.h.dpatch
    - copyright-without-copyright-notice: debian/copyright

 -- Mark Purcell <msp@debian.org>  Mon, 25 Aug 2008 21:26:37 +1000

libcommoncpp2 (1.6.2-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Tue, 22 Apr 2008 18:54:14 +1000

libcommoncpp2 (1.6.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 24 Mar 2008 21:45:37 +1100

libcommoncpp2 (1.6.0-1) unstable; urgency=low

  * New upstream release
  * Bump to libcommoncpp2-1.6-0 to match upstream soname
  * Cleanup get-orig-source VARS

 -- Mark Purcell <msp@debian.org>  Sat, 01 Dec 2007 20:36:12 +0100

libcommoncpp2 (1.5.9-1) unstable; urgency=low

  [ Kilian Krause ]
  * New upstream release.

 -- Mark Purcell <msp@debian.org>  Sat, 20 Oct 2007 11:29:05 +0100

libcommoncpp2 (1.5.7-2) unstable; urgency=high

  * Urgency high for fixing RC bug.
  * Remove bits/atomicity.h detection from sources (Closes: #439117, #441163)
  * Bump Standards-Version to 3.7.2.

 -- Kilian Krause <kilian@debian.org>  Sun, 09 Sep 2007 12:33:14 +0200

libcommoncpp2 (1.5.7-1) unstable; urgency=low

  * New upstream release.
  * Fix building twice in a row. (Closes: #424498)
  * Remove debian/patches/sp_is_running_always_true.dpatch: Applied upstream.
  * Convert Source-Version into binary:Version to allow binNMUs.
  * Bump Build-Depends on debhelper to satisfy cdbs debhelper.mk requirements.

 -- Kilian Krause <kilian@debian.org>  Sat, 11 Aug 2007 13:20:39 +0000

libcommoncpp2 (1.5.6-3) unstable; urgency=low

  * Actually apply gcc4.3 patch, Thanks Martin (Closes: #417329)

 -- Mark Purcell <msp@debian.org>  Sun, 15 Apr 2007 07:38:55 +0100

libcommoncpp2 (1.5.6-2) unstable; urgency=low

  * Patch from Stephan Suerken fixes
    - threads: isRunning() always returns true (Closes: #410219)
  * Patch from Martin Michlmayr fixes:
    - FTBFS with GCC 4.3: missing #includes (Closes: #417329)
  * Switch to cdbs

 -- Mark Purcell <msp@debian.org>  Thu, 12 Apr 2007 19:01:19 +0100

libcommoncpp2 (1.5.6-1) unstable; urgency=low

  * New upstream release
  * Remove debian/patches/kFreeBSD.dpatch encorporated upstream
  * Update debian/patches/soname-1.5.3.dpatch

 -- Mark Purcell <msp@debian.org>  Thu, 12 Apr 2007 16:22:05 +0100

libcommoncpp2 (1.5.5-1) unstable; urgency=low

  [ Mark Purcell ]
  * New upstream release
  * Patch from Peter Salinger (Closes: #403719: libcommoncpp2: FTBFS on
    GNU/kFreeBSD
  * Remove ccgnu2-config.8 from debian/patches/01_debian.dpatch
  * Cleanup gnutls.dpatch for new upstream version
  * delete debian/patches/no_udp_reuseaddr.dpatch - incorporated
    upstream
  * Cleanup debian/patches again

 -- Mark Purcell <msp@debian.org>  Sat, 17 Feb 2007 12:50:52 +0000

libcommoncpp2 (1.5.3-4) unstable; urgency=low

  * Fix typo in Depends: pkgconfig => pkg-config.

 -- Kilian Krause <kilian@debian.org>  Fri, 19 Jan 2007 11:13:55 +0100

libcommoncpp2 (1.5.3-3) unstable; urgency=low

  * Add depends on pkgconfig to libcommoncpp2-dev. (Closes: #407296)
  * Fix debhelper depends to compat level to shutup linda.

 -- Kilian Krause <kilian@debian.org>  Fri, 19 Jan 2007 00:01:06 +0100

libcommoncpp2 (1.5.3-2) unstable; urgency=medium

  * Urgency medium as this fixes a RC bug & has been discussed with debian-release
  * debian/patches/soname-1.5.3.dpatch - upstream is not ABI compatable - bump soname
    - twinkle: symbol lookup error when hanging up (Closes: #402009)

 -- Mark Purcell <msp@debian.org>  Sat,  9 Dec 2006 09:31:44 +0000

libcommoncpp2 (1.5.3-1) unstable; urgency=low

  * New upstream release
  * Build-Depends: autotools-dev & remove config.[sub,guess] changes
    from debian/patches
    - Fixes: reverting patch 01_debian from ./ ... fails durring rebuild (Closes:
    #399481)

 -- Mark Purcell <msp@debian.org>  Sun,  3 Dec 2006 12:03:19 +0000

libcommoncpp2 (1.5.1-4) unstable; urgency=low

  * Let's Add Build-Depends: libgnutls-dev, libgcrypt11-dev | libgcrypt-
    dev as well
    - missing dependency on libgnutls-dev (Closes: #396996)

 -- Mark Purcell <msp@debian.org>  Sat,  4 Nov 2006 10:40:45 +0000

libcommoncpp2 (1.5.1-3) unstable; urgency=low

  * Add Depends: libgnutls-dev, zlib1g-dev, libgcrypt11-dev | libgcrypt-
    dev
    - missing dependency on libgnutls-dev (Closes: #396996)

 -- Mark Purcell <msp@debian.org>  Sat,  4 Nov 2006 10:23:02 +0000

libcommoncpp2 (1.5.1-2) unstable; urgency=low

  [ Mikael Magnusson ]
  * Link libccext2 against ssl libraries
  * Disable REUSEADDR on UDP sockets

 -- Mark Purcell <msp@debian.org>  Wed, 25 Oct 2006 17:50:43 +0100

libcommoncpp2 (1.5.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun, 22 Oct 2006 21:00:31 +0100

libcommoncpp2 (1.5.0-1) unstable; urgency=low

  * New upstream release
  * Bump soname: package-name-doesnt-match-sonames libccext2-1.5-0
    libccgnu2-1.5-0

 -- Mark Purcell <msp@debian.org>  Mon,  2 Oct 2006 08:44:47 +0800

libcommoncpp2 (1.4.3-1) unstable; urgency=low

  * New upstream release
  * Lintian cleanup: manpage-has-useless-whatis-entry
    usr/share/man/man1/ccgnu2-config.1.gz

 -- Mark Purcell <msp@debian.org>  Sun,  3 Sep 2006 11:09:52 +0100

libcommoncpp2 (1.4.2-2) unstable; urgency=low

  * Thanks to Jonas Meyer & Amaya for picking up a typo in the
    debian/patches/
    - Fixes: makes other packages FTBFS with G++ 4.1: endKeydata was not declared
    in this scope (Closes: #356601)

 -- Mark Purcell <msp@debian.org>  Tue, 15 Aug 2006 22:37:34 +0100

libcommoncpp2 (1.4.2-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu,  6 Jul 2006 19:57:37 +0100

libcommoncpp2 (1.4.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Wed, 10 May 2006 18:29:14 +0100

libcommoncpp2 (1.4.0-1) unstable; urgency=low

  * New upstream release
  * Bump soname libcommoncpp2-1.4-0

 -- Mark Purcell <msp@debian.org>  Sun,  7 May 2006 11:08:08 +0100

libcommoncpp2 (1.3.26-4) unstable; urgency=low

  * ost_check2.m4 provided by -doc in i386 while by -dev in other arches
    (Closes: #366148)

 -- Mark Purcell <msp@debian.org>  Sat,  6 May 2006 00:08:57 +0100

libcommoncpp2 (1.3.26-3) unstable; urgency=low

  [ Kilian Krause ]
  * debian/rules: Don't build arch=all in binary-arch target.

  [ Mark Purcell ]
  * Add debian/patches/bug360492.dpatch. Fixes: pointer.h:55:
     error: invalid function declaration (Closes: #360492)
  * debian/control: Conflict and Replace with only libcommoncpp2-1.3c2a (1.3.26-1)

 -- Mark Purcell <msp@debian.org>  Tue, 18 Apr 2006 23:19:15 +0100

libcommoncpp2 (1.3.26-2) unstable; urgency=low

  * Bump soname

 -- Mark Purcell <msp@debian.org>  Thu, 13 Apr 2006 12:57:47 +0100

libcommoncpp2 (1.3.26-1) unstable; urgency=low

  [ Kilian Krause ]
  * debian/rules: Add get-orig-source target.

  [ Mark Purcell ]
  * New upstream release
  * debian/watch to use svn-upgrade
  * Update debian/patches/01_debian.dpatch

 -- Mark Purcell <msp@debian.org>  Wed, 12 Apr 2006 19:23:19 +0100

libcommoncpp2 (1.3.25-3) unstable; urgency=low

  * Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
  * Build-Depends: dpatch
  * libcommoncpp2-doc Architecture: all

 -- Mark Purcell <msp@debian.org>  Tue, 28 Mar 2006 22:24:10 +0100

libcommoncpp2 (1.3.25-2) unstable; urgency=low

  * Update libcommoncpp2-dev.manpages, libcommoncpp2-dev.dirs, libcommoncpp2-dev.examples
    - Cleanup debian/rules for the above
  * Update DH_COMPAT=4
  * Split off Architecture: any libcommoncpp2-doc package
  * Apply G++ 4.1 patch from Martin Michlmayr (Closes: Bug#356601)

 -- Mark Purcell <msp@debian.org>  Sun, 19 Mar 2006 14:35:45 +0000

libcommoncpp2 (1.3.25-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun, 12 Mar 2006 19:51:06 +0000

libcommoncpp2 (1.3.24-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 18 Feb 2006 12:18:28 +0000

libcommoncpp2 (1.3.22-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 21 Jan 2006 11:40:07 +0000

libcommoncpp2 (1.3.21-2) unstable; urgency=low

  * library package needs to be renamed (libstdc++ allocator change)
    (Closes: #339198)
  * Disable TODO in doc/Doxyfile
    - libcommoncpp2-dev: file conflict with libccrtp-dev:
    /usr/share/man/man3/todo.3.gz (Closes: #338397)

 -- Mark Purcell <msp@debian.org>  Mon, 21 Nov 2005 21:41:18 +0000

libcommoncpp2 (1.3.21-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 29 Oct 2005 14:37:39 +0100

libcommoncpp2 (1.3.19-1) unstable; urgency=low

  * New upstream release
  * Lintian cleanup old-fsf-address-in-copyright-file

 -- Mark Purcell <msp@debian.org>  Sun,  4 Sep 2005 22:35:08 +0100

libcommoncpp2 (1.3.14-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Tue, 19 Jul 2005 00:08:54 +1000

libcommoncpp2 (1.3.13-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu,  7 Jul 2005 05:11:57 +0100

libcommoncpp2 (1.3.10-4) unstable; urgency=low

  * Merge ubuntu changes for C++ ABI change (Thanks Stephen)
    - Please feel free to submit other ubuntu changes directly to the Debian
    BTS

 -- Mark Purcell <msp@debian.org>  Thu,  7 Jul 2005 04:00:50 +0100

libcommoncpp2 (1.3.10-3ubuntu1) breezy; urgency=low

  * Add Conflicts/Replaces: libcommoncpp2-1.0-0c102 (= 1.0.13-5ubuntu1)
  * debian/rules: moved test on config.{guess,sub} to configure

 -- Stephan Hermann <sh@sourcecode.de>  Mon, 06 Jun 2005 20:28:45 +0200

libcommoncpp2 (1.3.10-3) unstable; urgency=low

  * Add Conflicts/Replaces: libcommoncpp2-1.0-0c102 (=1.3.10-1) (Closes:
    Bug#311397)
  * Closes: #311149: FTBFS: dh_installdocs: cp: cannot stat `doc/html':
    No such file or directory
    - Add Build-Depends doxygen

 -- Mark Purcell <msp@debian.org>  Wed,  1 Jun 2005 22:41:13 +0100

libcommoncpp2 (1.3.10-2) unstable; urgency=low

  * soname change requires lib package bump to libcommoncp2-1.3

 -- Mark Purcell <msp@debian.org>  Sat, 28 May 2005 17:39:30 +0100

libcommoncpp2 (1.3.10-1) unstable; urgency=low

  * New upstream release
    - Closes: #264994: Underquoted AC_DEFUNs in automake macro
    - Closes: #294611: Underquoted definitions in m4
  * Add GENERATE_TODOLIST=NO & SHOW_DIRECTORIES=NO to doc/Doxyfile.in
  * Closes: #246529: Developer package should include html documentation

 -- Mark Purcell <msp@debian.org>  Sat, 28 May 2005 16:30:15 +0100

libcommoncpp2 (1.0.13-5) unstable; urgency=low

  * Apply gcc-3.4 patch from Andreas Jochens (Closes: Bug#259384)

 -- Mark Purcell <msp@debian.org>  Thu, 15 Jul 2004 08:17:20 +1000

libcommoncpp2 (1.0.13-4) unstable; urgency=low

  * ./reconf
  * Closes: Bug#251485: libcommoncpp2: FTBFS: old libtool version.

 -- Mark Purcell <msp@debian.org>  Thu,  1 Jul 2004 22:33:31 +1000

libcommoncpp2 (1.0.13-3) unstable; urgency=low

  * Unset CFLAGGS/CXXFLAGS for configure (Closes: Bug#234912)
  * Build ccgnu2-config.1 using help2man
  * Closes: #92368: /usr/include/cc++/config.h defines PACKAGE and
    VERSION
  * Closes: #179991: commonc++: Not fully conform to
    STDC++ (warnings with g++-3.2)

 -- Mark Purcell <msp@debian.org>  Fri,  5 Mar 2004 18:18:56 +1100

libcommoncpp2 (1.0.13-2) unstable; urgency=low

  * Update upstream URL in debian/control (Closes: Bug#233034)

 -- Mark Purcell <msp@debian.org>  Tue, 24 Feb 2004 11:50:43 +1100

libcommoncpp2 (1.0.13-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 18 Aug 2003 20:37:28 +1000

libcommoncpp2 (1.0.12-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu, 26 Jun 2003 21:31:23 +1000

libcommoncpp2 (1.0.10-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 24 May 2003 14:56:07 +1000

libcommoncpp2 (1.0.9-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu, 17 Apr 2003 19:46:00 +1000

libcommoncpp2 (1.0.8-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu, 13 Mar 2003 18:49:32 +1100

libcommoncpp2 (1.0.7-4) unstable; urgency=low

  * Adding throw() to nanosleep, Thanks Peter Toneby (Closes:
    Bug#176525)

 -- Mark Purcell <msp@debian.org>  Tue, 14 Jan 2003 20:06:35 +1100

libcommoncpp2 (1.0.7-3) unstable; urgency=low

  * Fixup unsigned int bug in include/c++/socket.h, Thanks Gerhard Tonn
    (Closes: Bug#173081)

 -- Mark Purcell <msp@debian.org>  Sun, 12 Jan 2003 23:54:16 +1100

libcommoncpp2 (1.0.7-2) unstable; urgency=low

  * Rebuild for gcc-3.2

 -- Mark Purcell <msp@debian.org>  Thu,  9 Jan 2003 22:12:54 +1100

libcommoncpp2 (1.0.7-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun,  8 Dec 2002 20:59:24 +1100

libcommoncpp2 (1.0.5-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun, 10 Nov 2002 20:08:17 +1100

libcommoncpp2 (1.0.3-2) unstable; urgency=low

  * Rebuild with standard g++

 -- Mark Purcell <msp@debian.org>  Sun,  6 Oct 2002 19:52:45 +1000

libcommoncpp2 (1.0.3-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun,  6 Oct 2002 19:08:45 +1000

libcommoncpp2 (1.0.2-2) unstable; urgency=low

  * Source Section: devel

 -- Mark Purcell <msp@debian.org>  Sun, 22 Sep 2002 21:15:43 +1000

libcommoncpp2 (1.0.2-1) unstable; urgency=low

  * New upstream release
  * Renamed Source: libcommoncpp2
  * Updated to Standards-Version: 3.5.2

 -- Mark Purcell <msp@debian.org>  Sun, 22 Sep 2002 13:08:14 +1000

commoncpp2 (1.0.0-1) unstable; urgency=low

  * New upstream release (Closes: Bug#154911)

 -- Mark Purcell <msp@debian.org>  Tue,  3 Sep 2002 20:45:49 +1000

commoncpp2 (0.99.6-3) unstable; urgency=low

  * New Maintainer.

 -- Mark Purcell <msp@debian.org>  Sun,  4 Aug 2002 10:29:11 +1000

commoncpp2 (0.99.6-2.1) unstable; urgency=low

  * NMU
  * Can't include <asm/atomic.h> in user space.  Especially with g++, since
    it has non-g++'isms in it.  Closes: #154911

 -- LaMont Jones <lamont@debian.org>  Tue, 30 Jul 2002 23:05:10 -0600

commoncpp2 (0.99.6-2) unstable; urgency=low

  * Fix g++-3.x problems.  Tested to compile cleanly with g++-3.1
    (Closes: #151888)

 -- Matt Zimmerman <mdz@debian.org>  Thu,  4 Jul 2002 11:37:53 -0400

commoncpp2 (0.99.6-1) unstable; urgency=low

  * New upstream release
  * I am neglecting this package somewhat because I no longer use it actively.
    If you do, and want to adopt it, contact me.

 -- Matt Zimmerman <mdz@debian.org>  Thu,  4 Jul 2002 00:19:27 -0400

commoncpp2 (0.99.2-1) unstable; urgency=low

  * Initial release (Closes: #142598)

 -- Matt Zimmerman <mdz@debian.org>  Fri, 15 Mar 2002 21:46:06 -0500
